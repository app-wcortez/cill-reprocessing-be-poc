package com.reprocessing.be.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CillReprocessingBeAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CillReprocessingBeAppApplication.class, args);
	}

}
