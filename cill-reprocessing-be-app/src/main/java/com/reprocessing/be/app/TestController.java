package com.reprocessing.be.app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class TestController {

	@Value("${cill.reprocessing.be.message:message property not found}")
	String message;
	
	@Value("${cill.reprocessing.be.username:username property not found}")
	String username;
	
	@Value("${cill.reprocessing.be.password:password property not found}")
	String password;
	
	@RequestMapping("/message")
	public String getMessage() {
		String messageReturn = String.format("message: %s || username: %s || password: %s", 
								message, username, password);
		return messageReturn;
	}
}
